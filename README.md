# Masm64 Library

fearless 2015 - [www.LetTheLight.in](http://www.LetTheLight.in)

## Overview

This library is a 64bit port of Steve Hutchesson's Masm32 library from www.masm32.com

## Whats included in this package
* Masm64.inc - Include file for Masm64 library for use with JWasm64
* Masm64.lib - Library file for Masm64 library for use with JWasm64
* Masm64.rap - RadASM project for JWasm64
* Masm64LibraryOnly.zip - Contains just the .inc and .lib
* Masm64LibrarySource.zip - Contains the RadASM project and source files
* Masm64Library-FullPackageIncSource.zip - Contains all of the files.

##Installation
* Copy Masm64.inc to \JWasm\include folder
* Copy Masm64.lib to \JWasm\x64 folder
* Include the Masm64 Library files in your JWasm x64 project
```
    include Masm64.inc
    includelib Masm64.lib
```

##Notes
* Original code from www.masm32.com MASM32.LIB modified to port it to x64.
* Not all functions have been ported.
* Not all functions have been tested.
* There may be differences from the orignal MASM32.LIB in usage for calling functions and expected return results. 
* This project is free for anyone to contribute to.

## Other downloads
 * [RadASM IDE v2.2.1.6](http://www.oby.ro/rad_asm/)
 * [RadASM IDE v2.2.2.0](http://www.assembly.com.br/download/radasm.html)
 * [JWasm/JWasm64](http://masm32.com/board/index.php?topic=3795.0)
 * [x64dbg plugin SDK for JWasm64](https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-jwasm64)
 * [x64dbg plugin SDK for Masm](https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-masm)

